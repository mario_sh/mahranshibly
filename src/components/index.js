
//This is your top level React component, you may change everything


import React from 'react'
import logo from '../assets/spotim-logo.jpg'
import {Container, Image} from 'semantic-ui-react'
import styled from 'styled-components';

import io from "socket.io-client";
const socket = io("https://spotim-demo-chat-server.herokuapp.com");



const Logo = styled.div`
      img{
        margin-left: auto;
        margin-right: auto;
        margin-top: 15px; 
		vertical-align: middle;
		padding : 5px;
		border-radius: 50px 15px;
      }
      
`;


class App extends React.Component {
    
	
	constructor(props){
       super(props);
       this.state = {
		   userNameDisabled: false,  // user must singnup to send messages
		   btnLoginDisabled: true,	 // hide btn (user name not empty)  
           fields: {},
           errors: {}
       }
    }


	handleValidationUsername(){
        
		let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

		
		
		//UserName	// error - "UserName cannot be empty!"  - to join chat;
		
		if(!fields["name"]){  
           this.setState({btnLoginDisabled: true});
		   formIsValid = false;
           errors["name"] = "UserName cannot be empty!";
        }
	
		// if all chars are letters - (my extra)
		if(typeof fields["name"] !== "undefined"){
           if(!fields["name"].match(/^[a-zA-Z]+$/)){
              formIsValid = false;
			  this.setState({btnLoginDisabled: true});
              errors["name"] = "User name must contain Only letters!";
           }        
        }
		
		if(fields["name"].length<1) {
			formIsValid = false;
			this.setState({btnLoginDisabled: true});
			errors["name"] = "User namecant be empty/empty_space ";
		}
	
		if (formIsValid) this.setState({btnLoginDisabled: false});
       
	   this.setState({errors: errors});
       return formIsValid;
   }
   

	handleValidationMessage(){
        
		let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        
		
		//message // "Message cannot be empty!" - to send sms;
        if(!fields["message"]){ 
           formIsValid = false;
           errors["message"] = "Message cannot be empty!";
        }
        /* //if all chars are letters (dont need )
		if(typeof fields["message"] !== "undefined"){
           if(!fields["message"].match(/^[a-zA-Z]+$/)){
              formIsValid = false;
              errors["message"] = "Only letters";
           }        
        }
		*/	
	   
       this.setState({errors: errors});
       return formIsValid;
   }
   
   
	messageSend(e){
        e.preventDefault();		
			//if(this.handleValidationUsername()){
			if(this.handleValidationMessage()){ // if msg & user Validation (not empty)
				
				//send msg after handleValidation 
				sendToServerByIOsocket("logo",this.state.fields["name"],this.state.fields["message"]);
				this.state.fields["message"]="";
			}
			//else{alert("Form has errors.")}
	}// alerts with invalid-feedback class (bootstrap)
	
    
	
	
	// handle message input Change - user cant send empty message./user name must
	handleChange1(field, e){         
        let fields = this.state.fields;
        fields[field] = e.target.value;        
        this.setState({fields});
    }

	
	
    //handle usenname input Change
	//just when user sign-up with username (login btn visibil)
	handleChange(field, e){
		this.handleChange1(field, e); 
		
		this.setState({btnLoginDisabled: false});
    }
	
		
	//user signup & login
	handleClickLogin = () => {
		if(this.handleValidationUsername()){
				this.setState({btnLoginDisabled: true}); // login btn Disabled - after join chat
				this.setState({userNameDisabled: true}); //// hide disabled username edit.. const user!.
				sendToServerByIOsocket("logo", this.state.fields["name"],"Hello, I Join Spot.IM-Chat. ");	
		}
	}
    render(){	
        return (
		<Container className={'spotim-header'}>
			<div className={'spotim-title'}>Welcome to the Spot.IM Chat app</div>
			
			<div>
				<Logo>
					<Image size={'tiny'} src={logo}/>
				</Logo>
			</div>
	 
			
			<div class="container form-group form-control"
			name="contactform" className="contactform" onSubmit= {this.messageSend.bind(this)}>
			
				<form >
	
	
					<div class="container form-group ">
						<div class="row form-group">
							<input id="user-name" type="text" class="input-box form-control form-group active is-invalid col-sm-6 col-md-6" placeholder="UserName - must"
							disabled={this.state.userNameDisabled} ref="name" type="text" onChange={this.handleChange.bind(this, "name")} value={this.state.fields["name"]}/>
							<button id="login_btn" type="button" class="btn btn-primary form-control form-group active col-sm-6 col-md-6"
							disabled={this.state.btnLoginDisabled} data-toggle="collapse" data-target="#demo"
							onClick={this.handleClickLogin}>Join Spot.IM-Chat-Room</button>
							<div class="invalid-feedback">
								<span style={{color: "red"}}>{this.state.errors["name"]}</span>
							</div>		
						</div>		
					</div>
	
	
					<div class="panel-group panel-control">
						<ul id="all-messages" class="container" ></ul>
					</div>
		
					
					<div class="container form-group is-invalid" >
						<div class="collapse row form-group panel-collapse collapse in" id="demo">					
							<input id="input_text" class="input-box form-control form-group active col-md-10 is-invalid" placeholder="Please... type a message.. and Send"
							ref="message" type="text" onChange={this.handleChange1.bind(this, "message")} value={this.state.fields["message"]}/>
					
							<button id="send_btn" value="Submit" disabled={!this.state.userNameDisabled}
							class="btn btn-primary form-control form-group active col-md-2">Send</button>
							
							<div class="invalid-feedback" >{this.state.errors["message"]}</div>
										
						</div>
					</div>
					
					
				</form>
			
			</div>
		</Container>
		)
	}
}


// Send Json to socket io
// send messages in the following format:
// {avatar:"...",username:"...",text:"...",time:"..."} 

// Send Json To Server By IO.socket ,emit='spotim/chat'
function sendToServerByIOsocket(avatarStr,usernameStr,textStr){
	socket.emit('spotim/chat', {
			avatar: avatarStr, 
			username: usernameStr,
			text: textStr,
			time: timeNow()
		});
}


// Time AM-PM
function timeNow(){
	
	var currentTime = new Date(),
		hours = currentTime.getHours(),
		minutes = currentTime.getMinutes();
	
	var suffix = "AM";
	if (minutes < 10)
		minutes = "0" + minutes;
	if (hours >= 12) {
		suffix = "PM";
		hours = hours - 12;
	}	
	if (hours === 0) hours = 12;
	return (hours + ":" + minutes + " " + suffix);
}

export default App;