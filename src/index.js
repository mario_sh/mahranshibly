//This is then entry point for your app. Do as you wish.
/*
Spot.IM Socket.IO chat server in order to send and receive messages.
Chat service endpoint:https://spotim-demo-chat-server.herokuapp.com
Event name: spotim/chat 
user sends a message you should use socket io 
to send messages in the following format:
{avatar:"...",username:"...",text:"...",time:"..."} 
(You may add additional fields if you like.)
listener for and render incoming chat messages.
*/

import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./components";
import io from "socket.io-client";


ReactDOM.render(<App />, document.getElementById("root"));

//connecting to Socket.IO chat server
const socket = io("https://spotim-demo-chat-server.herokuapp.com");

var	userName = document.getElementById('user-name'), 
    messages = document.getElementById('all-messages');



socket.on("connect", function() {
	console.log("connected to chat server!");
	
	messages.innerHTML +=
		'<li id="server-msg" class="list-group-item active"><strong id="strong">'+ 
		'Server : Hii, Please Sign-Up and join chat-room to send messages!.</strong></li>';
		
});


socket.on("disconnect", function() {
	console.log("disconnected from chat server!");
	
	messages.innerHTML += 
		'<li class="list-group-item list-group-item-danger"><strong>' + 
		 ' - Disconnected From Chat-Server.!!!</strong></li>';
	
});


// Event Listener for and render incoming chat messages.
// Rendered chat messages should show the avatar (if there is such), username and chat message.
// list containing incoming chat messages, including this.user messages


  


// user can read other users messages without-login ,
// but can't join chat -must signup  to send sms-.
socket.on('spotim/chat', function(data){
    
	//if(LOGIN===true)  
		// background color by bootstrap.	
		if(data.username===userName.value){ // user messages have different color 
			//loginName.disabled=true;
			messages.innerHTML += 
			'<li class="list-group-item list-group-item-info"><img src="static/media/spotim-logo.03ad5d99.jpg" class="avatar avatar-user"><strong> '+
			data.username +' : </strong> '+ data.text+
			' <span class="badge badge-light badge-pill float-right"> '+data.time+' </span> </li>';
		}else{ // all other users have same color
			messages.innerHTML += 
			'<li class="list-group-item list-group-item-success"><img src="static/media/spotim-logo.03ad5d99.jpg" class="avatar avatar"> <strong> '+ 
			data.username +' : </strong>'+ data.text +
			' <span class="badge badge-light badge-pill float-right"> '+data.time+' </span></li>';
		}
			
});